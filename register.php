<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reminder</title>
  <link rel="icon" href="images/favicon.png">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
  <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/skins/_all-skins.css"> 
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box" style="margin-top: 10px">
  <div class="register-logo"  style="margin-top: -3px">
    <a href=""><b>Reminder</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="register_logic.php" method="post" class="form-element">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="First Name" name="fname" required>
        <span class="ion ion-person form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Last name" name="lname" required>
        <span class="ion ion-person form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" required>
        <span class="ion ion-email form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Mobile" name="mobile" required>
        <span class="ion ion-email form-control-feedback "></span>
      </div>
      <!-- <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="pwd" required>
        <span class="ion ion-locked form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="cpwd" required>
        <span class="ion ion-log-in form-control-feedback "></span>
      </div> -->
      <div class="row">
        <div class="col-12">
          <div class="checkbox">
            <input type="checkbox" id="basic_checkbox_1">
			<label for="basic_checkbox_1">I agree to the <a href="#"><b>Terms</b></a></label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-orange btn-block btn-flat margin-top-10">SIGN UP</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    
     <div class="margin-top-20 text-center">
    	<p>Already have an account?<a href="index.php" class="text-info m-l-5"> Sign In</a></p>
     </div>
    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->


	<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
	<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
	<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	
</body>

</html>
