<?php 
include "config.php";
if (isset($_POST['varify']) && isset($_REQUEST['email']))
{
    $email = $_REQUEST['email'];
    $otp = $_POST["otp"];
    $check = "SELECT * FROM user WHERE otp='$otp' AND uemail='$email' ";
    $r = mysql_query($check, $conn);
    if (mysql_num_rows($r)>0) 
    {
        header('Location: recover_password.php?email='.$email);
    }
}


if (isset($_POST['varify'])) 
{
  $otp = $_POST["otp"];
  $check = "SELECT * FROM user WHERE otp='$otp' ";
  $r = mysql_query($check, $conn);
  if (mysql_num_rows($r)>0) 
  {
    $row=mysql_fetch_assoc($r);
    $email=$row['uemail'];
    $pwd=$row['upwd'];
    $fname=$row['ufname'];
    $lname=$row['ulname'];
    
    $sql = "UPDATE user SET otp_varified=1 WHERE otp='$otp' ";
    $result = mysql_query($sql,$conn);

    $from = "no-reply-reminder@maierp.in";
    $subject = "Registration successful";
    $message = " Dear ".$fname." ".$lname.",

  Congratulations! You have been registered successfully on Reminder App.
  Please go through the following details for login your account.

  "."Username : ".$email."
  Password : ".$pwd."(one time password)

  Thanks
  Team Reminder";
    $mail = mail($email, $subject, $message, "From: $from");


    header('Location: index.php?user_varified=yes');
  }
  else
  {
      header('Location: index.php?user_varified=no');
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reminder</title>
  <link rel="icon" href="images/favicon.png">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
  <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/skins/_all-skins.css"> 
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>
<body class="hold-transition register-page" style="background-color:#f96a31;">
<div class="register-box" style="margin-top: 10px">
  <div class="register-logo"  style="margin-top: -3px">
    <a href=""><b>Reminder</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Verify OTP sent on your mobile</p>

    <form action="varify_otp.php" method="post" class="form-element">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Enter OTP" name="otp" required>
        <span class="ion ion-person form-control-feedback "></span>
      </div>
      <div class="row">
        <div class="col-12">
          
        </div>
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" name="varify" class="btn btn-orange btn-block btn-flat margin-top-10">VERIFY</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    
     <div class="margin-top-20 text-center">
      <p>Already have an account?<a href="index.php" class="text-info m-l-5"> Sign In</a></p>
     </div>
    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->


  <script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor_components/popper/dist/popper.min.js"></script>
  <script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  
</body>

</html>
