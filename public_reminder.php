<?php
    session_start();
    if (!isset($_SESSION['id']))
    {
        header('Location: index.php');
    }
    include "config.php";
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" href="images/favicon.png">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php include("tital.php"); ?></title>
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
  <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/vendor_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/skins/_all-skins.css">	
  <link rel="stylesheet" href="css/jquery.datetimepicker.css">  
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <script src="js/sweetalert.js"></script>
  <link rel="stylesheet" href="css/sweetalert.css">

</head>
<body class="hold-transition skin-yellow-light sidebar-mini">
<div class="wrapper">


<?php include("header.php"); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
<?php include("menu.php"); ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <div class="row">
      <a href="home.php"> < Back </a>
    </div>
      <h2>Add Reminder</h2>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
        <li class="breadcrumb-item active">Add Reminder</li>
      </ol>
    </section>

<section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Default Basic Forms</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-12">
      <form name="reminder" action="public_reminder_logic.php" method="post">
        
        <div class="form-group row">
          <label for="example-email-input" class="col-sm-2 col-form-label">Reminder Type <label style="color: red">*</label></label>
          <div class="col-sm-10">
            <div class="radio" style="display: inline;">
              <input name="r_type" type="radio" id="Personal" value="Personal" checked="" class="radio-col-yellow">
              <label for="Personal">Personal</label>                    
            </div>
            <div class="radio" style="display: inline;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="r_type" type="radio" id="Public" value="Public" class="radio-col-yellow">
            <label for="Public">Public</label>   
            </div>
          </div>
        </div>  

        <div class="form-group row">
				  <label for="example-text-input" class="col-sm-2 col-form-label">Title <label style="color: red">*</label></label>
				  <div class="col-sm-10">
					<input class="form-control" type="text" placeholder="Enter title" id="title" name="title" autocomplete="off" required>
				  </div>
				</div>

				<div class="form-group row">
				  <label for="example-search-input" class="col-sm-2 col-form-label">Description <label style="color: red">*</label></label>
				  <div class="col-sm-10">
					<input class="form-control" type="search" placeholder="Enter Description" id="desc" name="desc" autocomplete="off" required>
				  </div>
				</div>

        <div class="form-group row">
          <label for="example-search-input" class="col-sm-2 col-form-label">Location <label style="color: red">*</label></label>
          <div class="col-sm-10">
          <input class="form-control" type="search" placeholder="Enter Location" id="location" name="location" autocomplete="off" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="example-datetime-local-input" class="col-sm-2 col-form-label">From Date <label style="color: red">*</label></label>
          <div class="col-sm-10">
          <input class="form-control" name="from_date" id="from_date" autocomplete="off" required placeholder="Reminder Start Date">
          </div>
        </div>

        <div class="form-group row">
          <label for="example-datetime-local-input" class="col-sm-2 col-form-label">End Date <label style="color: red">*</label></label>
          <div class="col-sm-10">
          <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" required placeholder="Reminder End Date" onchange="picker()">
          </div>
          
        </div>
        
        <button style="display: none;" class="btn btn-primary sweet-1" onclick="_gaq.push(['_trackEvent', 'example', 'try', 'sweet-1']);">Try It</button>
        <script type="text/javascript">
          function picker()
          {
              var date_ini = $('#from_date').val();
              var date_end = $('#to_date').val();
              if (date_ini > date_end) 
              {
                  swal({
                    title: "Warning..!",
                    text: "Invalid To Date! Please select To date greater than From Date",
                    type: "warning"
                  });
                  $('#to_date').val('');
                  return false;
              }

          }
          function picker2()
          {
              var date_ini = $('#from_date').val();
              var start_date = $('#start_date').val();
              if (date_ini <= start_date) 
              {
                  swal({
                    title: "Warning..!",
                    text: "Invalid Start Date! Please select Start Date Less than From Date",
                    type: "warning"
                  });
                  $('#start_date').val('');
                  return false;
              }
          }
        </script>

				<div class="form-group row">
				  <label for="example-email-input" class="col-sm-2 col-form-label">Reminder Require <label style="color: red">*</label></label>
				  <div class="col-sm-10">
            <div class="radio" style="display: inline;">
              <input name="r_require" type="radio" id="Yes" value="1" class="radio-col-yellow" onclick="show1()">
              <label for="Yes">Yes</label>                    
            </div>
            <div class="radio" style="display: inline;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="r_require" type="radio" id="No" value="0" class="radio-col-yellow" onclick="show2()">
            <label for="No">No</label>   
            </div>
				  </div>
				</div>

        <div id="rr" style="display: none;">
        <div class="form-group row">
          <label for="example-datetime-local-input" class="col-sm-2 col-form-label">Start Date<label style="color: red">*</label></label>
          <div class="col-sm-10">
          <input class="form-control" name="start_date" id="start_date" autocomplete="off" placeholder="Reminder Alert Start Date" onchange="picker2()">
          </div>
        </div>

        <div class="form-group row">
          <label for="example-datetime-local-input" class="col-sm-2 col-form-label">Time Periods<label style="color: red">*</label></label>
          <div class="col-sm-10">
            <select class="form-control select2" style="width: 100%;" name="period">
              <option value="">Select</option>
              <option value="24">Onces a day</option>
              <option value="12" >Twice a Day</option>
            </select>
          </div>
        </div>
        </div>

        <div class="form-group row">
          <label for="example-email-input" class="col-sm-2 col-form-label">Reminder Priority <label style="color: red">*</label></label>
          <div class="col-sm-10">
            <div class="radio" style="display: inline;">
              <input name="r_priority" type="radio" id="Low" value="Low" class="radio-col-yellow">
              <label for="Low">Low</label>                    
            </div>
            <div class="radio" style="display: inline;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="r_priority" type="radio" id="Medium" value="KMedium" class="radio-col-yellow">
            <label for="Medium">Medium</label>   
            </div>
            <div class="radio" style="display: inline;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="r_priority" type="radio" id="High" value="High" class="radio-col-yellow">
            <label for="High">High</label>   
            </div>
          </div>
        </div>

        

        <div class="form-group row" id="gp" style="display: none;">
          <label for="example-tel-input" class="col-sm-2 col-lg-2 col-form-label">Select Groups <label style="color: red">*</label></label>
          <div class="col-sm-9 col-lg-9">
            <select id="grp" class="form-control select2" data-placeholder="Choose Groups" style="width: 100%" name="grp[]">
            <?php
              $sql = "SELECT * FROM group_table";
              $result = mysql_query($sql,$conn);
              if (mysql_num_rows($result) > 0)
              {
                while ($row = mysql_fetch_assoc($result)) 
                {
                  $gname = $row['gname'];
                  $gid = $row['gid'];
                  ?>
                  <option value="<?= $gid ?>"><?= $gname ?></option>';
               <?php }
              }
            ?>
            </select>
          </div>
          <div class="col-sm-1 col-lg-1">
          <button onclick="group()" type="button" class="btn btn-lg btn-default text-left"><i class="fa fa-plus" ></i> Add </button>
          </div>
        </div>

        <script type="text/javascript">
          function group()
          {
            var aa = $('#grp').val();
            aa = aa.toString();
            var lastIndex = aa.lastIndexOf(",");
            var s1 = aa.substring(0, lastIndex); //after this s1="Text1, Text2, Text"
            var s2 = aa.substring(lastIndex + 1); //after this s2="true"

              var q = $('#part').val();
              
              if (q=="") 
              {
                q=0;
              }
              q=q.toString();
              // alert(q);

              $.post("getajax.php",{action:'contact_group',gid:s2, user:q}, function(data){
              $("#part").append(data);
              // alert(data);

  });
          }
        </script>

        <div class="form-group row" id="par" style="display: none;">
				<!-- <div class="form-group row" id="par"> -->
				  <label for="example-tel-input" class="col-sm-2 col-form-label">Participants <label style="color: red">*</label></label>
				  <div class="col-sm-10">
            <select id="part" class="form-control select2" multiple="multiple" data-placeholder="Choose Users" style="width: 100%" name="par[]" >
            <?php
              $sql = "SELECT * FROM user WHERE user_varified='Yes' ";
              $result = mysql_query($sql,$conn);
              if (mysql_num_rows($result) > 0)
              {
                while ($row = mysql_fetch_assoc($result)) 
                {
                  $uid = $row['userid'];
                  $fname = $row['ufname']; 
                  $lname = $row['ulname']; ?>
                  <option value="<?= $uid ?>"><?= $fname ?> <?= $lname ?></option>';
               <?php }
              }
            ?>
            </select>
				  </div>
				</div>

        

        <div id="field">
          <div id="field0">
            <div class="form-group row">
              <label  for="example-search-input" class="col-sm-2 col-form-label">Add Task</label>
              <div class="col-md-10">
                <input id="action_id" name="action_id[]" type="text" placeholder="enter task here" class="form-control input-md">
              </div>
            </div>
          </div>
        </div>
    
        <div class="form-group">
          <div class="col-md-4">
            <button id="add-more" name="add-more" class="btn btn-primary">Add Sub Task</button>
          </div>
        </div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () 
  {
    var next = 0;
    $("#add-more").click(function(e)
    {
      e.preventDefault();
      var addto = "#field" + next;
      var addRemove = "#field" + (next);
      next = next + 1;
      var newIn = 
      ' <div id="field'+ next +'" name="field'+ next +'"><div class="form-group row"><div class="col-md-12"> <input id="action_id" name="action_id[]" type="text" placeholder="enter sub task here" class="form-control input-md"> </div></div></div>';
      var newInput = $(newIn);
      var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me col-md-1">Remove</button></div><div id="field">';
      var removeButton = $(removeBtn);
      $(addto).after(newInput);
      $(addRemove).after(removeButton);
      $("#field" + next).attr('data-source',$(addto).attr('data-source'));
      $("#count").val(next);  

      $('.remove-me').click(function(e)
      {
        e.preventDefault();
        var fieldNum = this.id.charAt(this.id.length-1);
        var fieldID = "#field" + fieldNum;
        $(this).remove();
        $(fieldID).remove();
      });
    });

  }); 
</script>

				
            </div><!-- /.col -->
          </div><!-- /.row -->
          <hr>
          <div class="row">
            <div class="col-lg-12 col-xs-12">
                <center>
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="Reset" class="btn btn-warning"><a href="">Reset</a></button>
                  <button type="button" class="btn btn-danger">Cancel</button>
                </center>
            </div>
          </div>
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->  
    </section>
</div><!-- /.content-wrapper -->

  <footer class="main-footer">
    <?php include("footer.php"); ?>
  </footer>
  
  <div class="control-sidebar-bg"></div>
</div>

  <script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor_components/popper/dist/popper.min.js"></script>
  <script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="assets/vendor_components/select2/dist/js/select2.full.js"></script>
  <script src="js/pages/advanced-form-element.js"></script>
  <script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
  <script src="js/template.js"></script>
  <script src="js/demo.js"></script>

<!--   date time picker -->
  <script src="js/jquery.datetimepicker.full.min.js"></script>
  <script type="text/javascript">


    $(function () {
      $('#from_date').datetimepicker({ minDate: new Date() }); 
    });

    $(function () {
      $('#to_date').datetimepicker({ minDate: new Date() }); 
    });

    $(function () {
      $('#start_date').datetimepicker({ minDate: new Date() }); 
    });

    jQuery('#from_date').datetimepicker({
      format:'d.m.Y H:i:s'
    });

    jQuery('#to_date').datetimepicker({
      format:'d.m.Y H:i:s'
    });

    jQuery('#start_date').datetimepicker({
      format:'d.m.Y H:i:s'
    });
  </script>
<!--   date time picker -->

<script type="text/javascript">
  $("input[name='r_type']:radio").change(function() 
  {
      $("#par").toggle($(this).val() == "Public");
  });

  $("input[name='r_type']:radio").change(function() 
  {
      $("#gp").toggle($(this).val() == "Public");
  });


</script>



   <!-- dfsbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<button class="tst3 btn btn-success" style="display: none;"></button>
<button class="tst4 btn btn-danger" style="display: none;"></button>
<?php
if (isset($_GET['reminder']) && $_GET['reminder'] == "success")
{ ?>

  <script type="text/javascript">

    $(document).ready(function () {
      $.toast({
          heading: 'Reminder Added Successfully',
          text: 'you can see added reminder on calendar',
          position: 'top-right',
          loaderBg: '#ff6849',
          icon: 'success',
          hideAfter: 3500,
          stack: 6
      });

  });
  </script>

<?php }
else if(isset($_GET['reminder']) && $_GET['reminder'] == "fail")
{ ?>
    
  <script type="text/javascript">
   $(document).ready(function () {
        $.toast({
            heading: 'Reminder fail',
            text: 'Please try again later',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'error',
            hideAfter: 3500

        });

    });
  </script>

<?php  }
?>
<link rel="stylesheet" type="text/css" href="assets\vendor_components\jquery-toast-plugin-master\src\jquery.toast.css">
<script src="assets\vendor_components\jquery-toast-plugin-master\src\jquery.toast.js"></script>
<!-- dfsbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->

<!-- for show hide start date and time period based on rem_req -->
<script type="text/javascript">
  function show1(){document.getElementById('rr').style.display = 'block'; }
function show2(){document.getElementById('rr').style.display ='none';}
</script>
<!-- for show hide start date and time period based on rem_req -->

</body>
</html>