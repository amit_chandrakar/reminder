<?php
    session_start();
    if (!isset($_SESSION['id']))
    {
        header('Location: index.php');
    }    
    //echo $_SESSION['id'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="images/favicon.png">
  <title><?php include("tital.php"); ?></title>
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" /> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css">
  <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.css">
  <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.css">
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/skins/_all-skins.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
  <script src="assets/vendor_components/popper/dist/popper.min.js"></script>
  <script src="assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script> 
  <script src="assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
  <script src="assets/vendor_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> 
  <script src="assets/vendor_plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
  <script src="assets/vendor_components/raphael/raphael.min.js"></script>
  <script src="assets/vendor_components/morris.js/morris.min.js"></script>
  <script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>
  <script src="js/template.js"></script>
  <script src="assets/vendor_components/moment/moment.js"></script>

  <style type="text/css">
    .fc-day-grid-container.fc-scroller 
    {
      height: auto!important;
      overflow-y: auto;
    }
    .fc h2 
    {
      font-size: 15px;
    }
    .fc-day-number 
    {
      font-size: 10px;
      font-weight: 300;
    }
    .fc th.fc-widget-header 
    {
      color: #67757c;
      font-size: 10px;
      font-weight: 300;
      line-height: 20px;
      padding: 7px 0;
      font-weight: 300;
    }
    .fc-widget-content {
     /*border-color: rgba(120, 130, 140, .13)!important;*/
     border-color: 1px solid black!important;
      }
      .eo-fullcalendar{
    width:50%;    
}
</style>
<script>$('#cal').draggable();</script>
<script> 
  $('#cal').fullCalendar({
    height: "auto"
  });
  $(document).ready(function() {
   var calendar = $('#cal').fullCalendar({
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    fixedWeekCount:false,
    contentHeight:"auto",
    handleWindowResize:true,
    events: 'load.php',
    selectable:true,
    selectHelper:true,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:true,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
    },

    eventClick:function(event)
    {
     if(confirm("Are you sure you want to remove it?"))
     {
      var id = event.id;
      $.ajax({
       url:"delete.php",
       type:"POST",
       data:{id:id},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Event Removed");
       }
      })
     }
    },

   });
  });
   
  </script>
</head>

<body class="hold-transition skin-yellow-light sidebar-mini">
<div class="wrapper">

<?php include("header.php"); ?>
  
  <aside class="main-sidebar">
    <section class="sidebar">
        <?php include("menu.php"); ?>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
	  <h2>My Calendar</h2>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">My Calendar</li>
      </ol>
    </section>

    <!-- Main content Open -->
 <section class="content">
	 <div class="row">
        <div class="col-lg-9 col-md-12 col-xs-12">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <div id="cal"></div>
            </div>
          </div>
        </div>


        <div class="col-lg-3 col-md-12">
          <div class="box box-solid">
		   <div class="box-header with-border"><h4 class="box-title">Events</h4></div>
           <div class="box-body">
              <div id="external-events" >
        					<div class="external-event text-aqua dot-outline" data-class="bg-aqua"><i class="fa fa-hand-o-right"></i>Today</div>     
                  <div class="external-event text-green dot-outline" data-class="bg-green"><i class="fa fa-hand-o-right"></i>Sunday</div>
                  <div class="external-event text-red dot-outline" data-class="bg-red"><i class="fa fa-hand-o-right"></i>High Priority</div>
                  <div class="external-event text-purple dot-outline" data-class="bg-purple"><i class="fa fa-hand-o-right"></i>Medium Priority</div>
                  <div class="external-event text-yellow dot-outline" data-class="bg-yellow"><i class="fa fa-hand-o-right"></i>Low Priority</div>
              </div>
              <div class="event-fc-bt"><a href="#" data-toggle="modal" data-target="#add-new-events" class="btn btn-lg btn-orange btn-block margin-top-10"><i class="ti-plus"></i> Add New Event</a></div>
            </div>
          </div>
        </div>

      </div>
	  
	  
      <!-- BEGIN MODAL -->
	  <?php include("eventform.php"); ?>	
	  <!-- END MODAL -->
      
      <!-- /.row --> 
	  
	  
	       
	</section>
    <!-- Main content Close -->
  </div>
  
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("footer.php"); ?>
  </footer>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->

</body>
</html>
