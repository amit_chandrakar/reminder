<?php include("pushstate.php"); ?>
<ul class="sidebar-menu" data-widget="tree">
<?php
?>

  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/home.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('home.php')">
    <a href="#">
      <i class="fa fa-home"></i> <span>Home</span>
    </a>
  </li>



  <?php if ($_SESSION['utype']=="A") 
  { ?>
  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/users.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('users.php')">
    <a href="#">
      <i class="fa fa-users" aria-hidden="true"></i> <span>Users</span>
    </a>
  </li>
  <?php } ?>



  
  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/calendar.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('calendar.php')">
    <a href="#">
      <i class="fa fa-calendar" aria-hidden="true"></i><span>My Calendar</span>
    </a>
  </li>



  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/public_reminder.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('public_reminder.php')">
    <a href="#">
      <i class="fa fa-bell" aria-hidden="true"></i><span>Add Reminder</span>
    </a>
  </li>



  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/my_note.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('my_note.php')">
    <a href="#">
      <i class="fa fa-sticky-note" aria-hidden="true"></i><span>My Note</span>
    </a>
  </li>



  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/reports.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('reports.php')">
    <a href="#">
      <i class="fa fa-newspaper-o" aria-hidden="true"></i><span>Reports</span>
    </a>
  </li>



  <li class="treeview
  <?php
    if($_SERVER['PHP_SELF']=='/reminder/contact_list.php')
    { 
      echo 'active';
    } 
  ?>
  " onclick="FunUrl('contact_list.php')">
    <a href="#">
      <i class="fa fa-phone-square" aria-hidden="true"></i><span>Contact</span>
    </a>
  </li> 


  <li>
    <a download href="<?php if($_SESSION['utype']=="A"){echo"manual/admin_manual.pdf";}else{echo"manual/user_manual.pdf";} ?>">
      <i class="fa fa-file-text" aria-hidden="true"></i><span>Help File</span>
    </a>
  </li> 



</ul>
    
<script type="text/javascript">
function FunUrl(v){window.location=v;}
</script>	