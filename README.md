# Reminder

This is reminder web application which is available on web and play store.

The concept behind this project is to notify an individual or group of people for some specific notices regarding meetings, events, birthdays and anniversaries within their organization.

Follow the link to visit site. [https://reminder.amitchandrakar.com/]